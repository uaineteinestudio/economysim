# EconomySimulator

A C# economic simulator with resources, factories, buffs, national resource 'pools', with trade and supply/demand effects included.

**Note:** InDev.

## Getting Started

Find binaries in the [downloads](https://bitbucket.org/uaineteinestudio/economysim/downloads/) page.

Compling yourself: Use Visual Studio and the project file to create either a new project to be modified or add to a solution for use as a DLL.

### Requires:

This project is target for .NET standard 2.0. It should be compliant with earlier versions of course, however when compiling with VS, one will have to change the project settings.

The Console component is targeted for .NET framework 4.7.2.

### Built With

* [INIParser](https://bitbucket.org/uaineteinestudio/iniparser) by [UaineTeine](https://bitbucket.org/uaineteinestudio/) - The INI parser to read resource and factory types to simulate with
* [ConsoleWriter](https://bitbucket.org/uaineteinestudio/consolewriter) by [UaineTeine](https://bitbucket.org/uaineteinestudio/) - A console writer library for winforms to enable output to a textbox
* [VersionController](https://bitbucket.org/uaineteinestudio/versioncontroller) by [UaineTeine](https://bitbucket.org/uaineteinestudio/) - A version library
* [CSConsoleUI](https://bitbucket.org/uaineteinestudio/csconsoleui/) by [UaineTeine](https://bitbucket.org/uaineteinestudio/) - A UI library for C# consoles, required for easy colour switching
* [CSVHandler](https://bitbucket.org/uaineteinestudio/csvhandler/) by [UaineTeine](https://bitbucket.org/uaineteinestudio/) - A CSV reader library for C#
* [NameGenerator](https://bitbucket.org/uaineteinestudio/namegenerator/) by [UaineTeine](https://bitbucket.org/uaineteinestudio/) - A C# random name generator library

### Installing

##### Building with Windows:

Use Visual Studio to build the project file into an executable, deploy to the working directory of any future project you are making.

##### Building with Linux:

Open terminal/cmd and use .NET core to build with:

```
dotnet publish -c release -r ubuntu.16.04-x64 --self-contained
```

## Authors

* **Daniel Stamer-Squair** - *UaineTeine*

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details