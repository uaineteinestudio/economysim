﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CSC;

namespace EconSim.GUI
{
    static class warning
    {
        public static void give(string message)
        {
            UI.giveWarning(message);
            MessageBox.Show(message, "Warning",
            MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
