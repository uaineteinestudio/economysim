﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EconSim
{
    static class TraderHandler
    {
        public static void setupTrades()
        {
            //who wants trade
            for (int s1 = 0; s1 < econsim.n - 1; s1++)//last guy not needed as we shall have tried every combination already
            {
                if (econsim.nations[s1].readyfortradedeal())//is wanting deal so find second
                {
                    for (int s2 = 0; s2 < econsim.n; s2++)
                    {
                        if (s2 != s1 && econsim.nations[s2].readyfortradedeal())
                        {
                            tryDeals(s1, s2);
                        }
                    }
                }
            }
        }

        static void tryDeals(int s1, int s2)
        {
            int i = 0;
            int j = 0;
            while (i < econsim.nations[s1].wantedTrades.Count)//almost a for loop but index and array changing
            {
                while (j < econsim.nations[s2].wantedTrades.Count)
                {
                    bool dealstruck =
                        tryDeal(econsim.nations[s1].wantedTrades[i], econsim.nations[s2].wantedTrades[j],
                        ref econsim.nations[s1].itspool.reses, ref econsim.nations[s2].itspool.reses);
                    if (dealstruck)
                    {
                        econsim.nations[s1].removetrade(i);
                        i -= 1;
                        econsim.nations[s2].removetrade(j);
                        j -= 1;
                    }
                    j += 1;
                }
                i += 1;
            }
        }

        static bool tryDeal(wantedtrade s1, wantedtrade s2, ref resource[] s1res, ref resource[] s2res)
        {
            if (s2res[s1.resid].amount > s1.amount)//this is good
            {
                if (s1res[s2.resid].amount > s2.amount)//then we can trade
                {
                    //get value equal
                    float rat = gettraderat(s1, s2, out float a1, out float a2);
                    transaction newtrans = new transaction(s2.resid, a1, s1.resid,
                        a2, s1.whowantsthis, s2.whowantsthis);
                    newtrans.makeTrade(ref s1res, ref s2res);
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        private static float gettraderat(wantedtrade s1, wantedtrade s2, out float a1, out float a2)  //return rat of value s2/s1 and the amount s1.
        {
            float val2 = resTypes.reses[s2.resid].getval();
            float val1 = resTypes.reses[s1.resid].getval();
            float val = val2 / val1;
            a1 = s1.amount;
            a2 = a1 * val;
            return val;
        }
    }
}
