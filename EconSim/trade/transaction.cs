﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EconSim
{

    class transaction
    {
        int giveThis;
        int takeThat;
        float giveamt;
        float takeamt;

        public transaction(int gi, float ga, int ti, float ta, int fm, int to)
        {
            giveThis = gi;
            takeThat = ti;
            giveamt = ga;
            takeamt = ta;
        }

        internal void makeTrade(ref resource[] from, ref resource[] to)
        {
            from[giveThis].add(-giveamt);
            to[giveThis].add(giveamt);
            from[takeThat].add(takeamt);
            to[takeThat].add(-takeamt);
        }
    }
}
