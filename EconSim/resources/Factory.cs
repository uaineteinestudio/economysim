﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EconSim
{
    public class defFactory
    {
        public int type;
        internal int[] resourcesreq;       //res required
        internal int numresreq;            //
        internal int output;               //output index
        internal float production;         //output amount
        internal float[] consumptions;     //consumption amount
        internal string n;                 //name

        public defFactory()
        {
            //default constructor
            type = -1;
            resourcesreq = new int[] { 1 };
            numresreq = 1;
            consumptions = new float[] { -1f };
            production = -1f;
            output = -1;
            n = "blank fac";
        }
        public defFactory(int t, int[] indexes, int numres, float[] consum, float prod, int outpt, string name)
        {
            type = t;
            resourcesreq = indexes;
            numresreq = numres;
            consumptions = consum;
            production = prod;
            output = outpt;
            n = name;
        }

        public defFactory(int i, string iniline)
        {
            List<int> res = new List<int>();
            List<float> c = new List<float>();

            string[] split = iniline.Split(',');
            type = i;
            n = split[0];

            if (split.Length == 3)
            {
                output = Convert.ToInt32(split[2]);
                production = Convert.ToSingle(split[1]);
            }
            else
            {
                //indexes
                string[] s = split[1].Split('.');
                foreach (string ind in s)
                    res.Add(Convert.ToInt32(ind));

                //consumptions
                s = split[2].Split('.');
                foreach (string con in s)
                    c.Add(Convert.ToSingle(con));

                //prodution amount
                production = Convert.ToSingle(split[3]);
                output = Convert.ToInt32(split[4]);
            }
            resourcesreq = res.ToArray();
            numresreq = res.Count;
            consumptions = c.ToArray();
        }

        public string getname()
        {
            return n;
        }

        internal void addcons(ref float[] consPerTick)
        {
            for (int i = 0; i < numresreq; i++)
            {
                int c = resourcesreq[i];
                consPerTick[c] += consumptions[i];
            }
        }

        internal void addcreat(ref float[] creationPerTick)
        {
            creationPerTick[output] += production;
        }
    }

    public class Factory : defFactory   //inherit
    {
        private string n;               //name
        private int level = 1;          //level

        public Factory(int t, int[] indexes, int numres, float[] consum, float prod, int outpt, string name, int lvl) : 
            base(t, indexes, numres, consum, prod, outpt, name)
        {
            level = lvl;
            type = t;
            resourcesreq = indexes;
            numresreq = numres;
            consumptions = consum;
            production = prod;
            output = outpt;
            n = name;
            LevelFactor();
        }
        public Factory(int i, string iniline) : base(i, iniline)
        {
            level = 1;
            LevelFactor();//times by level
        }

        public string[] getFacStatusString()
        {
            return new string[] {
                     n,
                    level.ToString(),
                    getConsumesString(),
                    OutputString()
                };
        }

        public string getConsumesString()
        {
            StringBuilder sb = new StringBuilder();
            int n = consumptions.Length;
            for (int i = 0; i < n; i++)
            {
                int ind = resourcesreq[i];
                sb.Append(resTypes.reses[ind].name);
                sb.Append("(" + consumptions[i].ToString() + ") ");
                if (i < n - 1)
                    sb.Append(", ");
            }
            return sb.ToString();
        }

        public string OutputString()
        {
            StringBuilder sb = new StringBuilder(resTypes.reses[output].name);
            sb.Append("(" + production.ToString() + ")");
            return sb.ToString();
        }

        private void LevelFactor()
        {
            for (int i = 0; i < numresreq; i++)
            {
                consumptions[i] = resTypes.facs[type].consumptions[i] * level;
            }
            production = resTypes.facs[type].production * level;
        }

        public bool FactoryRun(in resource[] reses, out int whatIwant, out float howMuch)
        {
            whatIwant = 0;
            howMuch = 0;
            for (int i = 0; i < numresreq; i++)
            {
                int k = resourcesreq[i];
                float diff = consumptions[i] - reses[k].getAmount();
                if (diff > 0)//more consumption
                {
                    whatIwant = k;
                    howMuch = diff;
                    return false;
                }
            }
            //else
            return true;
        }

        public void levelUp()
        {
            level += 1;
        }

        public int getlvl()
        {
            return level;
        }

        internal void demoteSize()
        {
            level -= 1;
        }
    }
}
