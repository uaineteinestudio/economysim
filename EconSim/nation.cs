﻿using System;
using System.Collections.Generic;
using System.Text;
using Namelists;

namespace EconSim
{
    public class nation
    {
        protected int id;
        public resPool itspool;
        pop[] workers;
        internal string Name;
        bool canTrade;
        internal List<wantedtrade> wantedTrades = new List<wantedtrade>();

        public nation()
        {
            Name = "nameless";
            id = econsim.n;
            itspool = new resPool(resTypes.getDefResources(), resTypes.getDefFactories());
            canTrade = true;
        }
        public nation(int i, string nm)//assuming all default
        {
            itspool = new resPool(resTypes.getDefResources(), resTypes.getDefFactories());
            Name = nm;
            canTrade = true;
            id = i;
        }
        public nation(int i, string nm, resPool newpool, bool trd)
        {
            itspool = newpool;
            Name = nm;
            canTrade = trd;
            id = i;
        }

        public void Tick()
        {
            itspool.Tick();
            if (canTrade)
                checkTrade();//check if we want to trade
        }

        internal void removetrade(int index)
        {
            wantedTrades.RemoveAt(index);
        }

        public bool readyfortradedeal()//quickly checks the list of pending trade
        {
            if (wantedTrades.Count > 0)
                return true;
            else
                return false;
        }

        private bool checkTrade()
        {
            bool want = false;
            for (int i = 0; i < itspool.getFacLen(); i++)
            {
                bool canrun = itspool.facs[i].FactoryRun(in itspool.reses, out int wantres, out float amt);
                if (canrun == false)//if factory can't run
                {
                    //randomise a buy factor
                    float buyfac = RandFac.getFac(0.2f);
                    amt += buyfac * amt;//add this percent
                    float val = resTypes.reses[wantres].getval() * amt;
                    wantedTrades.Add(new wantedtrade(wantres, amt, val, id));
                    want = true;
                }
            }
            return want;
        }
    }

    public class randnation : nation
    {
        public randnation(int i)
        {
            string townname = Names.GenerateTownName();
            Random r = new Random();
            int type = r.Next() % Names.nationtypes;
            Name = Names.GetNationName(townname, type);
            id = i;

            //now to randomise that starting dosh
            resource[] reses = resTypes.getDefResources();
            Factory[] facs = resTypes.getDefFactories();

            for (int h = 0; h < reses.Length; h++)
            {
                float loss = RandFac.getFac(60);
                reses[h].amount = 100 - loss;
            }
            //might not have factories you know

            itspool = new resPool(reses, facs);
        }
    }
}
