﻿using System;
using System.Collections.Generic;
using System.Text;
using INI;

namespace EconSim
{
    public class resPool
    {
        public resource[] reses;
        private static int rn = 0;//len

        public Factory[] facs;
        private int fn = 0;//len

        private float[] consPerTick;     //consumptions per tick
        private float[] creationPerTick;

        public resPool(resource[] myres, Factory[] myFacs)
        {
            reses = myres;
            rn = myres.Length;
            facs = myFacs;
            fn = myFacs.Length;
            //now get my rates
            CalcPool();
        }

        public int getFacLen()
        {
            return fn;
        }

        internal void printStats()
        {
            int displ = 15;
            Console.WriteLine("Stats:");
            for (int i = 0; i < rn; i++)
            {
                Console.Write(reses[i].name);
                for (int j = 0; j < displ - reses[i].name.Length; j++)
                    Console.Write(" ");
                Console.Write(reses[i].getAmount().ToString());
                Console.Write(Environment.NewLine);
            }
        }

        public void Tick()
        {
            for(int i = 0; i < rn; i++)
            {
                reses[i].consume(consPerTick[i]);
                reses[i].produce(creationPerTick[i]);
            }
        }

        public void CalcPool()
        {
            consPerTick = new float[rn];
            creationPerTick = new float[rn];
            for (int i = 0; i < rn; i++)
            {
                consPerTick[i] = 0;
                creationPerTick[i] = 0;
            }
            for (int i = 0; i < facs.Length; i++)
            {
                if (facs[i].FactoryRun(in reses, out int wantres, out float howmuch))//enough to run me
                {
                    facs[i].addcons(ref consPerTick);
                    facs[i].addcreat(ref creationPerTick);
                }
            }
        }
    }
}
