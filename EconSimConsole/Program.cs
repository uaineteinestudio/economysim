﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EconSim;

namespace EconSim.Cons
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //initalise it
            econsim.initalise();
            Console.Read();
        }

        public static void Simulate(int numSteps)
        {
            econsim.Simulate(numSteps);
            econsim.printStats();
        }
    }
}
