﻿using INI;
using System;
using System.Collections.Generic;
using System.Text;

namespace EconSim
{
    public static class JobHandler
    {
        public static defPop[] defaultWorkers = new defPop[200];
        static int nWorkers = 0;
        static string fnm = "workerList.ini";

        private static string splitWorkerEntry(string line, out int[] canwork)
        {
            string[] split = line.Split('.');
            int n = split.Length - 1;
            canwork = new int[n];
            for (int i = 0; i < n; i++)
            {
                int k = i + 1;
                canwork[i] = Convert.ToInt32(split[k]);
            }
            return split[0]; //return thy name
        }
        public static void LoadWorkers()
        {
            IniParser pars = new IniParser(fnm);
            pars.Read();

            List<defPop> tmplist = new List<defPop>();
            int i = 0;
            while (true)
            {
                bool readSuccess = false;
                string key = "j" + i.ToString();
                string resInfo = pars.getDatFromKey("workinglist", key, out readSuccess);
                if (readSuccess == false)
                    break;
                else
                {
                    int[] canwork;
                    string name = splitWorkerEntry(resInfo, out canwork);
                    tmplist.Add(new defPop(name, canwork));
                }
                i += 1;
            }

            //now append this list to our jobs
            defaultWorkers = tmplist.ToArray();
            nWorkers = tmplist.Count;
        }
    }
}
