﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EconSim
{
    public class defPop
    {
        internal string name;
        internal int[] canWorkAt;
        public defPop(string n, int[] canwork)
        {
            name = n;
            canWorkAt = canwork;
        }

        public string[] getWorkerStatusString()
        {
            string[] stat = new string[2];
            stat[0] = name;
            StringBuilder sb = new StringBuilder();
            int n = canWorkAt.Length;
            for (int i = 0; i < n; i++)
            {
                int j = canWorkAt[i];
                if (i < n - 1)
                    sb.Append(resTypes.facs[j].n + ", ");
                else
                    sb.Append(resTypes.facs[j].n);
            }
            return stat;
        }
    }
    class pop
    {
        internal string name;
        int size;
        public pop(defPop thispop, int s)
        {
            size = s;
            name = thispop.name;
        }
    }
}
