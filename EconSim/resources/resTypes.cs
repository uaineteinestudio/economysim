﻿using System;
using System.Collections.Generic;
using System.Text;
using INI;

namespace EconSim
{
    public static class resTypes
    {
        public static defResource[] reses;
        private static int rn = 0;//len

        public static float[] vals;

        public static defFactory[] facs;
        private static int fn = 0;//len

        internal static void Initialise()
        {
            Console.WriteLine("Initalising pool...");
            //read my file list
            ReadRes(IO.resfn);
        }

        public static Factory[] getDefFactories()
        {
            int n = facs.Length;
            Factory[] f = new Factory[n];
            for (int i = 0; i < n; i++)
            {
                int newlvl = 1;
                f[i] = new Factory(i, facs[i].resourcesreq, facs[i].numresreq, facs[i].consumptions, facs[i].production, facs[i].output, facs[i].n, newlvl);
            }
            return f;
        }

        public static resource[] getDefResources()
        {
            int n = reses.Length;
            resource[] r = new resource[n];
            for (int i = 0; i < n; i++)
            {
                r[i] = new resource(i, reses[i].amount, reses[i].name);
            }
            return r;
        }

        private static void ReadRes(string fnm)
        {
            List<defResource> tmplist = new List<defResource>();

            IniParser pars = new IniParser(fnm);
            pars.Read();
            int i = 0;
            while (true)
            {
                bool readSuccess = false;
                string key = "r" + i.ToString();
                string resInfo = pars.getDatFromKey("reslist", key, out readSuccess);
                if (readSuccess == false)
                    break;
                else
                {
                    tmplist.Add(new defResource(i, resInfo));
                }
                i += 1;
            }
            reses = tmplist.ToArray();
            rn = tmplist.Count;

            List<defFactory> f = new List<defFactory>();
            i = 0;
            while (true)
            {
                bool readSuccess = false;
                string key = "f" + i.ToString();
                string resInfo = pars.getDatFromKey("reslist", key, out readSuccess);
                if (readSuccess == false)
                    break;
                else
                {
                    f.Add(new defFactory(i, resInfo));
                }
                i += 1;
            }
            facs = f.ToArray();
            fn = f.Count;

            Console.WriteLine("Reslist read");
        }
    }
}
