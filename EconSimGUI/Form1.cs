﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CSC;
using ConsoleWriter;

namespace EconSim.GUI
{
    public partial class Form1 : Form
    {
        TextWriter consoleRedirect;
        int SelectedNation = 0;
        bool WorldSelectedEventAllow = true;
        public Form1()
        {
            InitializeComponent();
            toolStripStatusLabel1.Text = "";
            label5.Text = "";
            label7.Text = "";
            consoleRedirect = new TextBoxStreamWriter(textBox1);
            Console.SetOut(consoleRedirect);

            //Eexcute that console
            EconSim.Cons.Program.Main(new string[] { "" });

            UpdateWorldComboBoxes();

            updateResList();
            updateFacList();
            updateWorkerList();

            //time update
            updateTime();
        }

        private void updateFacList()
        {
            listView4.Items.Clear();
            Factory[] defFacs = EconSim.resTypes.getDefFactories();
            for (int i = 0; i < EconSim.resTypes.facs.Length; i++)
            {
                string[] entry = defFacs[i].getFacStatusString();
                ListViewItem itm = new ListViewItem(
                    new string[] { entry[0], entry[2], entry[3], i.ToString() }
                    );
                listView4.Items.Add(itm);
            }
        }

        private void updateResList()
        {
            listView3.Items.Clear();
            for (int i = 0; i < EconSim.resTypes.reses.Length; i++)
            {
                string[] entry = EconSim.resTypes.reses[i].getResStatusString();
                ListViewItem itm = new ListViewItem(
                    new string[] { entry[0], entry[3], i.ToString() }
                    );
                listView3.Items.Add(itm);
            }
        }

        private void updateWorkerList()
        {
            listView5.Items.Clear();
            for (int i = 0; i < EconSim.JobHandler.defaultWorkers.Length; i++)
            {
                string[] entry = EconSim.JobHandler.defaultWorkers[i].getWorkerStatusString();
                ListViewItem itm = new ListViewItem(
                    new string[] { entry[0], entry[1], i.ToString() });
                listView5.Items.Add(itm);
            }
        }

        private void updateTime()
        {
            toolStripStatusLabel2.Text = econsim.gettime().ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        //fill in those boxes
        private void UpdateWorldComboBoxes()
        {
            ClearWorldBoxes();
            //for the world box
            string[] names = EconSim.econsim.getNationNames();
            foreach (string name in names)
            {
                comboBox1.Items.Add(name);
            }
            //update the labels above
            label5.Text = SelectedNation.ToString();
            label7.Text = names[SelectedNation]; 
        }
        private void ClearWorldBoxes()
        {
            comboBox1.Items.Clear();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (WorldSelectedEventAllow)
            {
                WorldSelectedEventAllow = false;
                SelectedNation = comboBox1.SelectedIndex;
                UpdateWorldComboBoxes();
                comboBox1.SelectedIndex = SelectedNation;
                WorldSelectedEventAllow = true;
                UpdateBoxes();
            }
        }

        private void UpdateBoxes()
        {
            toolStripProgressBar1.Value = 0;
            int count = 0;
            toolStripStatusLabel1.Text = "Getting resources";
            //list view
            listView1.Items.Clear();
            listView2.Items.Clear();

            int n = econsim.nations[SelectedNation].itspool.reses.Length;
            for (int i = 0; i < n; i++)
            {
                ListViewItem item = new ListViewItem(econsim.nations[SelectedNation].itspool.reses[i].getResStatusString());
                listView1.Items.Add(item);
                count += 1;
                toolStripProgressBar1.Value = (100 * count) / n;
            }
            toolStripProgressBar1.Value = 0;
            toolStripStatusLabel1.Text = "";

            //now for factories
            count = 0;
            toolStripStatusLabel1.Text = "Getting factories";
            n = econsim.nations[SelectedNation].itspool.facs.Length;
            for (int i = 0; i < n; i++)
            {
                ListViewItem item = new ListViewItem(econsim.nations[SelectedNation].itspool.facs[i].getFacStatusString());
                listView2.Items.Add(item);
                count += 1;
                toolStripProgressBar1.Value = (100 * count) / n;
            }
            toolStripProgressBar1.Value = 0;
            toolStripStatusLabel1.Text = "";
        }

        static string ConvertStringArrayToString(string[] array)
        {
            if (array.Length == 0)
                return "none";
            // Concatenate all the elements into a StringBuilder.
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
                builder.Append(',');
            }
            return builder.ToString();
        }

        static string ConvertIntArrayToString(List<int> array)
        {
            if (array.Count == 0)
                return "none";
            // Concatenate all the elements into a StringBuilder.
            StringBuilder builder = new StringBuilder();
            foreach (int value in array)
            {
                builder.Append(value.ToString());
                builder.Append(',');
            }
            return builder.ToString();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            UpdateStatusBar("Saving", 0);
            //Konigreich.IO.SaveLoad.SaveCurrentWorld();
            UpdateStatusBar("", 0);
        }

        private void statusStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        private void UpdateStatusBar(string label, int value)
        {
            toolStripProgressBar1.Value = value;
            toolStripStatusLabel1.Text = label;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            toolStripProgressBar1.Value = 0;
            toolStripStatusLabel1.Text = "Simulating...";
            int steps = 1;
            try
            {
                steps = Convert.ToInt32(textBox2.Text);
            }
            catch
            {
                steps = 1;
                string warn = "Textbox does not contain an integer, using steps=1";
                warning.give(warn);
            }
            EconSim.Cons.Program.Simulate(steps);
            toolStripStatusLabel1.Text = "";
            updateTime();
        }
    }
}
