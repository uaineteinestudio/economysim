﻿using System;
using System.Collections.Generic;
using System.Text;
using Namelists;

namespace EconSim
{
    public static class econsim
    {
        const int maxNations = 100;
        public static nation[] nations = new nation[maxNations];
        internal static int n = 0;
        internal static int time = 0;

        public static string[] getNationNames()
        {
            string[] names = new string[n];
            for (int i = 0; i < n; i++)
            {
                names[i] = nations[i].Name;
            }
            return names;
        }

        public static void printStats()
        {
            for (int i = 0; i < n; i++)
            {
                nations[i].itspool.printStats();
            }
            Console.WriteLine();
        }

        public static int gettime()
        {
            return time;
        }

        public static void initalise()
        {
            //load names
            Names.Load();

            //load workers
            JobHandler.LoadWorkers();

            resTypes.Initialise();
            addNation(new randnation(n));
        }

        public static void Tick()
        {
            for (int i = 0; i < n; i++)
                nations[i].Tick();
            TraderHandler.setupTrades();
        }

        public static void StepSimulate()
        {
            for (int j = 0; j < n; j++)
            {
                Tick();
                //nations[j].itspool.printStats();
            }
            time += 1;//one step
            //Console.WriteLine();
        }

        public static void Simulate(int numSteps)
        {
            //now simulate it
            for (int i = 0; i < numSteps; i++)
            {
                StepSimulate();
            }
        }

        public static void addNation(nation newn)
        {
            nations[n] = newn;                  //add to current index
            n += 1;                             //increment amount
        }
    }
}
