﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EconSim
{
    public class resource
    {
        protected int type;
        public string name;
        internal float amount;

        public resource()
        {
            type = -1;
            name = "unamed";
            amount = 0;
        }
        public resource(int t, float am, string nam)
        {
            type = t;
            amount = am;
            name = nam;
        }

        public resource(int t, string iniline)
        {
            string[] split = iniline.Split(',');
            type = t;
            amount = 100;
            name = split[0];
            //val = Convert.ToSingle(split[1]);-obslete for this
        }

        public void add(float giveamt)
        {
            amount += giveamt;
            zerocheck();
        }

        private bool zerocheck()            //return true if zeroed
        {
            if (amount < 0)
            {
                amount = 0;
                return true;
            }
            else return false;
        }

        public float getAmount()
        {
            return amount;
        }

        internal void consume(float v)
        {
            amount -= v;
            zerocheck();
        }

        internal void produce(float v)
        {
            amount += v;
        }

        public float getval()
        {
            return resTypes.reses[type].val;
        }

        public string[] getResStatusString()
        {
            float v = getval();
            float val = v * amount;
            return new string[] {
                    name,
                    amount.ToString(),
                    val.ToString(),
                    v.ToString()};
        }
    }

    public class defResource : resource
    {
        internal float val;

        public defResource(int t, string iniline)
        {
            string[] split = iniline.Split(',');
            type = t;
            amount = 100;
            name = split[0];
            val = Convert.ToSingle(split[1]);
        }
    }
}
