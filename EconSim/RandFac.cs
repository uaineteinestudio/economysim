﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EconSim
{
    internal static class RandFac
    {
        static Random r = new Random();
        internal static float getFac(float max)//out of maximum 1
        {
            return (float)(r.NextDouble()*max);
        }
    }
}
