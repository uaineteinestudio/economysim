﻿using VersionController;
using CSC;

namespace EconSim
{
    public static class VC
    {
        static Version curVers = new Version(new int[] { 1, 0, 0 }, "beta", true);

        public static void WriteVersion()
        {
            UI.white();
            System.Console.Write("Current Version: ");
            UI.yellow();
            string stringval = curVers.ToStr();
            System.Console.Write(stringval);
            System.Console.Write(System.Environment.NewLine);
            UI.white();
        }

        public static int compareToVersion(Version a)
        {
            return curVers.compareVersion(a);
        }

        public static string WriteVersionNoOnly()
        {
            Version tmpv = curVers;
            tmpv.changeDevStatus(false);
            return tmpv.ToStr();
        }
    }
}
